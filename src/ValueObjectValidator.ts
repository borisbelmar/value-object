import { ValidatorProps } from './types'
import { isNullOrUndefined } from './utils/isNullOrUndefined'
import { arrayValidate, booleanValidate, dateValidate, isRequired, numberValidate, objectValidate, stringValidate } from './validations'
import getArrayValidators, { ArrayValidators } from './validators/getArrayValidators'
import getBooleanValidators, { BooleanValidators } from './validators/getBooleanValidators'
import getDateValidators, { DateValidators } from './validators/getDateValidators'
import getNumberValidators, { NumberValidators } from './validators/getNumberValidators'
import getObjectValidators, { ObjectValidators } from './validators/getObjectValidators'
import getStringValidators, { StringValidators } from './validators/getStringValidators'

export class ValueObjectValidator<T> {
  private readonly props: ValidatorProps<T>

  constructor (value: T, valueObject: string) {
    this.props = { value, valueObject }
  }

  required = (errorMessage?: string): this => {
    isRequired({ ...this.props, errorMessage })
    return this
  }

  string = (errorMessage?: string): StringValidators => {
    if (!isNullOrUndefined(this.props.value)) {
      stringValidate({ ...this.props, errorMessage })
    }
    return getStringValidators(this.props as unknown as ValidatorProps<string>)
  }

  number = (errorMessage?: string): NumberValidators => {
    if (!isNullOrUndefined(this.props.value)) {
      numberValidate({ ...this.props, errorMessage })
    }
    return getNumberValidators(this.props as unknown as ValidatorProps<number>)
  }

  date = (errorMessage?: string): DateValidators => {
    if (!isNullOrUndefined(this.props.value)) {
      dateValidate({ ...this.props, errorMessage })
    }
    return getDateValidators(this.props as unknown as ValidatorProps<Date>)
  }

  boolean = (errorMessage?: string): BooleanValidators => {
    if (!isNullOrUndefined(this.props.value)) {
      booleanValidate({ ...this.props, errorMessage })
    }
    return getBooleanValidators(this.props as unknown as ValidatorProps<boolean>)
  }

  array = (errorMessage?: string): ArrayValidators<unknown> => {
    if (!isNullOrUndefined(this.props.value)) {
      arrayValidate({ ...this.props, errorMessage })
    }
    return getArrayValidators(this.props as unknown as ValidatorProps<unknown[]>)
  }

  object = (errorMessage?: string): ObjectValidators => {
    if (!isNullOrUndefined(this.props.value)) {
      objectValidate({ ...this.props, errorMessage })
    }
    return getObjectValidators(
      this.props as unknown as ValidatorProps<Record<string, unknown>>
    )
  }
}
