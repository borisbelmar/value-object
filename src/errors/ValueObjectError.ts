export default class ValueObjectError<T> extends Error {
  public value: T
  public valueObject: string

  constructor (value: T, valueObject: string, message: string) {
    super(message)
    this.value = value
    this.valueObject = valueObject
  }
}
