import { ValidatorProps } from '../../types'

const getMockProps = <T>(value: T): ValidatorProps<T> => ({
  valueObject: 'TestValueObject',
  value
})

export default getMockProps
