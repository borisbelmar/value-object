import { isRequired } from './isRequired'
import { ValueObjectError } from '../errors'
import getMockProps from './__mocks__/getMockProps.mock'

const mockUndefined = getMockProps(undefined)
const mockNull = getMockProps(null)
const mockEmptyString = getMockProps('')
const mockEmptyArray = getMockProps([])
const mockEmptyObject = getMockProps({})
const mockFalse = getMockProps(false)
const mockString = getMockProps('mock')
const mockZero = getMockProps(0)

describe('Testing objectHasKeys', () => {
  it('Must pass the validation', () => {
    isRequired(mockEmptyArray)
    isRequired(mockEmptyObject)
    isRequired(mockFalse)
    isRequired(mockString)
    isRequired(mockZero)
  })
  it('Must fail de validation', () => {
    try {
      isRequired(mockUndefined)
      isRequired(mockNull)
      isRequired(mockEmptyString)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
