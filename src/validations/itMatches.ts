import { ValueObjectError } from '../errors'
import { ValidatorProps } from '../types'

const itMatches = (props: ValidatorProps<string>, toMatch: RegExp): void => {
  if (!toMatch.test(props.value)) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_MUST_MATCH_{${toMatch.toString()}}`
    )
  }
}

export default itMatches
