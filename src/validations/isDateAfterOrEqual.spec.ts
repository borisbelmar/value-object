import { ValueObjectError } from '../errors'
import getMockProps from './__mocks__/getMockProps.mock'
import isDateAfterOrEqual from './isDateAfterOrEqual'

const mockDate = getMockProps(new Date('2021-09-05T23:31:33.498Z'))

describe('Testing isDateAfterOrEqual', () => {
  it('Must pass the validation', () => {
    isDateAfterOrEqual(mockDate, new Date('2021-09-04T23:31:33.498Z'))
    isDateAfterOrEqual(mockDate, new Date('2021-09-05T23:31:33.498Z'))
  })
  it('Must fail de validation', () => {
    try {
      isDateAfterOrEqual(mockDate, new Date('2021-09-06T23:31:33.498Z'))
      expect(false).toBe(true)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
