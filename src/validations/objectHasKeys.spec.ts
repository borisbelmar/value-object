import { objectHasKeys } from './objectHasKeys'
import { ValueObjectError } from '../errors'
import getMockProps from './__mocks__/getMockProps.mock'

const mockProps = getMockProps({ a: 'a', b: 'b' })

describe('Testing objectHasKeys', () => {
  it('Must pass the validation', () => {
    objectHasKeys(mockProps, ['a', 'b'])
  })
  it('Must fail de validation', () => {
    try {
      objectHasKeys(mockProps, ['a', 'b', 'c'])
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
