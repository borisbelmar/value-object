import { ValueObjectError } from '../errors'
import getMockProps from './__mocks__/getMockProps.mock'
import { isOneOf } from './isOneOf'

const mockNumber = getMockProps(1)
const mockString = getMockProps('b')

describe('Testing isOneOf', () => {
  it('Must pass the validation', () => {
    isOneOf(mockNumber, [1, 2, 3])
    isOneOf(mockString, ['a', 'b', 'c'])
  })
  it('Must fail de validation', () => {
    try {
      isOneOf(mockNumber, [2, 3, 4])
      isOneOf(mockString, ['a', 'c', 'd'])
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
