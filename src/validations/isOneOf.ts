import { ValidatorProps } from '../types'
import { ValueObjectError } from '../errors'

export const isOneOf = <T>(props: ValidatorProps<T>, array: T[]): void => {
  if (!array.includes(props.value as T)) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `${props.valueObject}_MUST_BE_ONE_OF_{${array.join(', ')}}`
    )
  }
}
