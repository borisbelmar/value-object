import { ValueObjectError } from '../errors'
import getMockProps from './__mocks__/getMockProps.mock'
import isEmail from './isEmail'

const mockEmail = getMockProps('valid@email.com')
const mockNonEmail = getMockProps('non@email')

describe('Testing isEmail', () => {
  it('Must pass the validation', () => {
    isEmail(mockEmail)
  })
  it('Must fail de validation', () => {
    try {
      isEmail(mockNonEmail)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
