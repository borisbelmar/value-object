import { ValueObjectError } from '../errors'
import { ValidatorProps } from '../types'

export const objectHasKeys = (
  props: ValidatorProps<Record<string, unknown>>,
  keys: string[]
): void => {
  if (!keys.every(key => Object.keys(props.value).includes(key))) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_OBJECT_MUST_HAVE_KEYS_{${keys.toString()}}`
    )
  }
}
