import { ValueObjectError } from '../errors'
import { ValidatorProps } from '../types'

export const isEqual = <T>(props: ValidatorProps<T>, toCompare: T): void => {
  if (!(props.value === toCompare)) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_MUST_BE_EQUAL_{${toCompare}}`
    )
  }
}

export const numberMin = (props: ValidatorProps<number>, min: number): void => {
  if (props.value < min) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_CANT_BE_LESSER_THAN_{${min}}`
    )
  }
}

export const numberMax = (props: ValidatorProps<number>, max: number): void => {
  if (props.value > max) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_CANT_BE_GREATER_THAN_${max}`
    )
  }
}

export const iterableMin = (props: ValidatorProps<unknown[] | string>, min: number): void => {
  if (props.value.length < min) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_CANT_HAVE_LESS_THAN_{${min}}_ELEMENTS`
    )
  }
}

export const iterableMax = (props: ValidatorProps<unknown[] | string>, max: number): void => {
  if (props.value.length > max) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_CANT_HAVE_MORE_THAN_{${max}}_ELEMENTS`
    )
  }
}
