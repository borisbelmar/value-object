import { compareAsc } from 'date-fns'
import { ValueObjectError } from '../errors'
import { ValidatorProps } from '../types'

const isDateAfterOrEqual = (props: ValidatorProps<Date>, toCompare: Date): void => {
  if (compareAsc(props.value, toCompare) < 0) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_DATE_CANT_BE_LESSER_THAN_{${toCompare}}`
    )
  }
}

export default isDateAfterOrEqual
