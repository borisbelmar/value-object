import { is } from 'ramda'
import { ValidatorProps } from '../types'
import { ValueObjectError } from '../errors'

export const stringValidate = (props: ValidatorProps<unknown>): void => {
  if (!is(String, props.value)) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_INVALID_STRING`
    )
  }
}

export const numberValidate = (props: ValidatorProps<unknown>): void => {
  if (!is(Number, props.value)) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_INVALID_NUMBER`
    )
  }
}

export const arrayValidate = (props: ValidatorProps<unknown>): void => {
  if (!is(Array, props.value)) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_INVALID_ARRAY`
    )
  }
}

export const booleanValidate = (props: ValidatorProps<unknown>): void => {
  if (!is(Boolean, props.value)) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_INVALID_BOOLEAN`
    )
  }
}

export const objectValidate = (props: ValidatorProps<unknown>): void => {
  if (!is(Object, props.value)) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_INVALID_OBJECT`
    )
  }
}

export const dateValidate = (props: ValidatorProps<unknown>): void => {
  if (!is(Date, props.value)) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_INVALID_DATE`
    )
  }
}
