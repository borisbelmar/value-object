import { ValueObjectError } from '../errors'
import { ValidatorProps } from '../types'
import { ValueObjectValidator } from '../ValueObjectValidator'

export const arrayContains = <T>(props: ValidatorProps<T[]>, toContain: T): void => {
  if (!props.value.includes(toContain)) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_ARRAY_MUST_CONTAINS_{${toContain}}`
    )
  }
}

export const arrayDeepEqual = (
  props: ValidatorProps<unknown[]>,
  toCompare: unknown[]
): void => {
  if (!(props.value.toString() === toCompare.toString())) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_ARRAY_MUST_BE_EQUAL_TO_{${toCompare.toString()}}`
    )
  }
}

export const arrayValidateEach = <T>(
  props: ValidatorProps<T[]>,
  cb: (validator: ValueObjectValidator<T>) => void
): void => {
  props.value.forEach((item: T) => {
    const validator = new ValueObjectValidator(item, props.valueObject)
    cb(validator)
  })
}
