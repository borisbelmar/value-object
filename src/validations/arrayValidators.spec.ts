import { ValueObjectError } from '../errors'
import { arrayContains, arrayDeepEqual, arrayValidateEach } from './arrayValidators'
import getMockProps from './__mocks__/getMockProps.mock'

const mockArray = getMockProps(['a', 'b', 'c'])

describe('Testing arrayContains', () => {
  it('Must pass the validation', () => {
    arrayContains(mockArray, 'a')
    arrayContains(mockArray, 'b')
  })
  it('Must fail de validation', () => {
    try {
      arrayContains(mockArray, 'd')
      arrayContains(mockArray, 'e')
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})

describe('Testing arrayDeepEqual', () => {
  it('Must pass the validation', () => {
    arrayDeepEqual(mockArray, ['a', 'b', 'c'])
  })
  it('Must fail de validation', () => {
    try {
      arrayDeepEqual(mockArray, ['b', 'c', 'a'])
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})

describe('Testing arrayValidateAll', () => {
  it('Must pass the validation', () => {
    arrayValidateEach(mockArray, validator => validator.string())
  })
  it('Must fail de validation', () => {
    try {
      arrayValidateEach(mockArray, validator => validator.number())
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
