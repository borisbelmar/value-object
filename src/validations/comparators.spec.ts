import { ValueObjectError } from '../errors'
import { isEqual, iterableMax, iterableMin, numberMax, numberMin } from './comparators'
import getMockProps from './__mocks__/getMockProps.mock'

const mockNumber = getMockProps(3)
const mockString = getMockProps('sol')
const mockArray = getMockProps(['a', 'b', 'c'])

describe('Testing isEqual', () => {
  it('Must pass the validation', () => {
    isEqual(mockNumber, 3)
    isEqual(mockString, 'sol')
  })
  it('Must fail de validation', () => {
    try {
      isEqual(mockNumber, 2)
      isEqual(mockString, 'so')
      isEqual(mockArray, ['a', 'b', 'c'])
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})

describe('Testing numberMax', () => {
  it('Must pass the validation', () => {
    numberMax(mockNumber, 3)
  })
  it('Must fail de validation', () => {
    try {
      numberMax(mockNumber, 2)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})

describe('Testing numberMin', () => {
  it('Must pass the validation', () => {
    numberMin(mockNumber, 3)
  })
  it('Must fail de validation', () => {
    try {
      numberMin(mockNumber, 4)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})

describe('Testing iterableMax', () => {
  it('Must pass the validation', () => {
    iterableMax(mockString, 3)
    iterableMax(mockArray, 3)
  })
  it('Must fail de validation', () => {
    try {
      iterableMax(mockString, 2)
      iterableMax(mockArray, 2)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})

describe('Testing iterableMin', () => {
  it('Must pass the validation', () => {
    iterableMin(mockString, 3)
    iterableMin(mockArray, 3)
  })
  it('Must fail de validation', () => {
    try {
      iterableMin(mockString, 4)
      iterableMin(mockArray, 4)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
