import { ValueObjectError } from '../errors'
import getMockProps from './__mocks__/getMockProps.mock'
import isDateBefore from './isDateBefore'

const mockDate = getMockProps(new Date('2021/05/01'))

describe('Testing isDateBefore', () => {
  it('Must pass the validation', () => {
    isDateBefore(mockDate, new Date('2021/05/02'))
  })
  it('Must fail de validation', () => {
    try {
      isDateBefore(mockDate, new Date('2021/04/30'))
      expect(false).toBe(true)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
