import { ValidatorProps } from '../types'
import { ValueObjectError } from '../errors'
import { isNullOrUndefined } from '../utils/isNullOrUndefined'

export const isRequired = (props: ValidatorProps<unknown>): void => {
  if (isNullOrUndefined(props.value)) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_IS_REQUIRED`
    )
  }
}
