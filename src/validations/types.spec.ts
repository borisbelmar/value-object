import {
  stringValidate,
  arrayValidate,
  objectValidate,
  numberValidate,
  booleanValidate,
  dateValidate
} from './types'
import { ValueObjectError } from '../errors'
import getMockProps from './__mocks__/getMockProps.mock'

const mockString = getMockProps('1')
const mockNumber = getMockProps(1)
const mockDate = getMockProps(new Date())
const mockBoolean = getMockProps(false)
const mockArray = getMockProps([])
const mockObject = getMockProps({})

describe('Testing types validations', () => {
  it('Must pass the validation', () => {
    stringValidate(mockString)
    numberValidate(mockNumber)
    arrayValidate(mockArray)
    objectValidate(mockObject)
    dateValidate(mockDate)
    booleanValidate(mockBoolean)
  })
  it('Must fail de validation', () => {
    try {
      stringValidate(mockNumber)
      numberValidate(mockString)
      arrayValidate(mockString)
      objectValidate(mockBoolean)
      dateValidate(mockString)
      booleanValidate(mockString)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
