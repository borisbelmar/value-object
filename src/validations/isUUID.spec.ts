import isUUID from './isUUID'
import { ValueObjectError } from '../errors'
import getMockProps from './__mocks__/getMockProps.mock'

const mockPropsUUID = getMockProps('646e2560-e544-4f19-aaa4-a40c37a1aa30')
const mockPropsNotUUID = getMockProps('not_uuid')

describe('Testing isUUID', () => {
  it('Must pass the validation', () => {
    isUUID(mockPropsUUID)
  })
  it('Must fail de validation', () => {
    try {
      isUUID(mockPropsNotUUID)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
