import { ValueObjectError } from '../errors'
import getMockProps from './__mocks__/getMockProps.mock'
import isDateAfter from './isDateAfter'

const mockDate = getMockProps(new Date('2021/05/01'))

describe('Testing isDateAfter', () => {
  it('Must pass the validation', () => {
    isDateAfter(mockDate, new Date('2021/04/30'))
  })
  it('Must fail de validation', () => {
    try {
      isDateAfter(mockDate, new Date('2021/05/10'))
      expect(false).toBe(true)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
