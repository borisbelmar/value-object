import { ValueObjectError } from '../errors'
import getMockProps from './__mocks__/getMockProps.mock'
import isDateBeforeOrEqual from './isDateBeforeOrEqual'

const mockDate = getMockProps(new Date('2021-08-28T23:31:33.498Z'))

describe('Testing isDateBeforeOrEqual', () => {
  it('Must pass the validation', () => {
    isDateBeforeOrEqual(mockDate, new Date('2021-09-29T23:31:33.498Z'))
    isDateBeforeOrEqual(mockDate, new Date('2021-08-28T23:31:33.498Z'))
  })
  it('Must fail de validation', () => {
    try {
      isDateBeforeOrEqual(mockDate, new Date('2021-08-27T23:31:33.498Z'))
      expect(false).toBe(true)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
