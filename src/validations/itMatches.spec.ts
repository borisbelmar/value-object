import itMatches from './itMatches'
import { ValueObjectError } from '../errors'
import getMockProps from './__mocks__/getMockProps.mock'

const mockProps = getMockProps('asA2d_asdA2')

describe('Testing itMatches', () => {
  it('Must pass the validation', () => {
    itMatches(mockProps, /^[a-zA-Z0-9_]*$/)
  })
  it('Must fail de validation', () => {
    try {
      itMatches(mockProps, /^[a-zA-Z]*$/)
      itMatches(mockProps, /^[a-z0-9]*$/)
    } catch (err) {
      expect(err).toBeInstanceOf(ValueObjectError)
    }
  })
})
