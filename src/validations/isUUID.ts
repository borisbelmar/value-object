import { v4 } from 'is-uuid'
import { ValueObjectError } from '../errors'
import { ValidatorProps } from '../types'

const isUUID = (props: ValidatorProps<string>): void => {
  if (!v4(props.value as string)) {
    throw new ValueObjectError(
      props.value,
      props.valueObject,
      props?.errorMessage || `{${props.valueObject}}_MUST_BE_VALID_UUID`
    )
  }
}

export default isUUID
