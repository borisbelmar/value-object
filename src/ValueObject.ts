import { ValueObjectError } from './errors'

export abstract class ValueObject<T> {
  protected value: T

  protected constructor (value: T) {
    this.value = value
  }

  static create (_props: unknown): unknown {
    throw new ValueObjectError(null, ValueObject.name, 'CREATE_METHOD_NOT_IMPLEMENTED')
  }

  public getValue (): T {
    return this.value
  }
}
