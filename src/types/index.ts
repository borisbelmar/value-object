export interface ValidatorProps<T> {
  value: T
  valueObject: string
  errorMessage?: string
  not?: boolean
}
