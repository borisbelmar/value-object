import { ValidatorProps } from '../types'
import { isOneOf } from '../validations/isOneOf'
import { isEqual } from '../validations/comparators'
import { isDateAfter, isDateAfterOrEqual, isDateBefore, isDateBeforeOrEqual } from '../validations'
import { isNullOrUndefined } from '../utils/isNullOrUndefined'

export interface DateValidators {
  min: (date: Date, errorMessage?: string) => this
  after: (date: Date, errorMessage?: string) => this
  max: (date: Date, errorMessage?: string) => this
  before: (date: Date, errorMessage?: string) => this
  oneOf: (array: Date[], errorMessage?: string) => this
  equal: (toCompare: Date, errorMessage?: string) => this
}

const getDateValidators = (props: ValidatorProps<Date>): DateValidators => {
  const min = (date: Date, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      isDateAfterOrEqual({ ...props, errorMessage }, date)
    }
    return getDateValidators(props)
  }

  const max = (date: Date, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      isDateBeforeOrEqual({ ...props, errorMessage }, date)
    }
    return getDateValidators(props)
  }

  const after = (date: Date, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      isDateAfter({ ...props, errorMessage }, date)
    }
    return getDateValidators(props)
  }

  const before = (date: Date, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      isDateBefore({ ...props, errorMessage }, date)
    }
    return getDateValidators(props)
  }

  const oneOf = (array: Date[], errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      isOneOf({ ...props, errorMessage }, array)
    }
    return getDateValidators(props)
  }

  const equal = (toCompare: Date, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      isEqual({ ...props, errorMessage }, toCompare)
    }
    return getDateValidators(props)
  }

  return {
    min,
    max,
    oneOf,
    equal,
    after,
    before
  }
}

export default getDateValidators
