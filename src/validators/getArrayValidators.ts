import { ValidatorProps } from '../types'
import { isNullOrUndefined } from '../utils/isNullOrUndefined'
import { iterableMax, iterableMin, arrayContains, arrayDeepEqual, arrayValidateEach } from '../validations'
import { ValueObjectValidator } from '../ValueObjectValidator'

export interface ArrayValidators<T> {
  deepEqual: (array: unknown[], errorMessage?: string) => this
  contains: (toContain: T, errorMessage?: string) => this
  validateEach: (cb: (validator: ValueObjectValidator<T>) => void, errorMessage?: string) => this
  min: (num: number, errorMessage?: string) => this
  max: (num: number, errorMessage?: string) => this
}

const getArrayValidators = <T>(props: ValidatorProps<T[]>): ArrayValidators<T> => {
  const min = (num: number, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      iterableMin({ ...props, errorMessage }, num)
    }
    return getArrayValidators(props)
  }

  const max = (num: number, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      iterableMax({ ...props, errorMessage }, num)
    }
    return getArrayValidators(props)
  }

  const deepEqual = (array: unknown[], errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      arrayDeepEqual({ ...props, errorMessage }, array)
    }
    return getArrayValidators(props)
  }

  const contains = (toContain: T, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      arrayContains({ ...props, errorMessage }, toContain)
    }
    return getArrayValidators(props)
  }

  const validateEach = (
    cb: (validator: ValueObjectValidator<T>) => void,
    errorMessage?: string
  ) => {
    if (!isNullOrUndefined(props.value)) {
      arrayValidateEach({ ...props, errorMessage }, cb)
    }
    return getArrayValidators(props)
  }

  return {
    min,
    max,
    deepEqual,
    contains,
    validateEach
  }
}

export default getArrayValidators
