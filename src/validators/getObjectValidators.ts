import { ValidatorProps } from '../types'
import { isNullOrUndefined } from '../utils/isNullOrUndefined'
import { objectHasKeys } from '../validations'

export interface ObjectValidators {
  hasKeys: (keys: string[]) => this
}

const getObjectValidators = (props: ValidatorProps<Record<string, unknown>>): ObjectValidators => {
  const hasKeys = (keys: string[], errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      objectHasKeys({ ...props, errorMessage }, keys)
    }
    return getObjectValidators(props)
  }

  return {
    hasKeys
  }
}

export default getObjectValidators
