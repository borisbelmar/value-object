import { ValidatorProps } from '../types'
import { isNullOrUndefined } from '../utils/isNullOrUndefined'
import { isEqual } from '../validations/comparators'

export interface BooleanValidators {
  equal: (toCompare: boolean, errorMessage?: string) => this
}

const getBooleanValidators = (props: ValidatorProps<boolean>): BooleanValidators => {
  const equal = (toCompare: boolean, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      isEqual({ ...props, errorMessage }, toCompare)
    }
    return getBooleanValidators(props)
  }

  return {
    equal
  }
}

export default getBooleanValidators
