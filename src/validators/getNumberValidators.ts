import { ValidatorProps } from '../types'
import { isOneOf } from '../validations/isOneOf'
import { isEqual, numberMax, numberMin } from '../validations/comparators'
import { isNullOrUndefined } from '../utils/isNullOrUndefined'

export interface NumberValidators {
  min: (num: number, errorMessage?: string) => this
  max: (num: number, errorMessage?: string) => this
  oneOf: (array: number[], errorMessage?: string) => this
  equal: (toCompare: number, errorMessage?: string) => this
}

const getNumberValidators = (props: ValidatorProps<number>): NumberValidators => {
  const min = (num: number, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      numberMin({ ...props, errorMessage }, num)
    }
    return getNumberValidators(props)
  }

  const max = (num: number, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      numberMax({ ...props, errorMessage }, num)
    }
    return getNumberValidators(props)
  }

  const oneOf = (array: number[], errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      isOneOf({ ...props, errorMessage }, array)
    }
    return getNumberValidators(props)
  }

  const equal = (toCompare: number, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      isEqual({ ...props, errorMessage }, toCompare)
    }
    return getNumberValidators(props)
  }

  return {
    min,
    max,
    oneOf,
    equal
  }
}

export default getNumberValidators
