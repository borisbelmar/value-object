import { ValidatorProps } from '../types'
import {
  isEmail,
  isUUID,
  itMatches,
  isOneOf,
  iterableMax,
  iterableMin
} from '../validations'
import { isNullOrUndefined } from '../utils/isNullOrUndefined'

export interface StringValidators {
  email: (errorMessage?: string) => this
  uuid: (errorMessage?: string) => this
  min: (num: number, errorMessage?: string) => this
  max: (num: number, errorMessage?: string) => this
  oneOf: (array: string[], errorMessage?: string) => this
  matches: (regex: RegExp, errorMessage?: string) => this
}

const getStringValidators = (props: ValidatorProps<string>): StringValidators => {
  const email = (errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      isEmail({ ...props, errorMessage })
    }
    return getStringValidators(props)
  }

  const uuid = (errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      isUUID({ ...props, errorMessage })
    }
    return getStringValidators(props)
  }

  const matches = (regex: RegExp, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      itMatches({ ...props, errorMessage }, regex)
    }
    return getStringValidators(props)
  }

  const min = (num: number, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      iterableMin({ ...props, errorMessage }, num)
    }
    return getStringValidators(props)
  }

  const max = (num: number, errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      iterableMax({ ...props, errorMessage }, num)
    }
    return getStringValidators(props)
  }

  const oneOf = (array: string[], errorMessage?: string) => {
    if (!isNullOrUndefined(props.value)) {
      isOneOf({ ...props, errorMessage }, array)
    }
    return getStringValidators(props)
  }

  return {
    email,
    uuid,
    min,
    max,
    matches,
    oneOf
  }
}

export default getStringValidators
