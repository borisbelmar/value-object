# ValueObject

Abstract ValueObject class for DDD apps. It includes validation.

## Quick start

```typescript
export interface UniqueIdProps {
  id: string
}

export default class UniqueId extends ValueObject<string> {
  static create (props: UniqueIdProps): UniqueId {
    const validator = new ValueObjectValidator(props.id, UniqueId.name)
    validator.required().string().uuid()
    return new UniqueId(props.id)
  }
}
```

## Coming features

- [ ] Tests
- [x] Regex string validation
- [ ] Custom validations
- [ ] Improve documentation